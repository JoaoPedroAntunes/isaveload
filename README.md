# ISaveLoad

Interface to be used for any vehicle dynamics model

## Getting Started

Clone the project and use the ISaveLoad object for your models.

### Prerequisites

```
None
```


## Using the project

The project allows to quickly add a save and load function using json. Pretty handy to
save multiples vehicle dynamic objects.


## Authors

* **Joao Antunes** - *Initial work* - [Gitlab](https://gitlab.com/JoaoPedroAntunes)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

