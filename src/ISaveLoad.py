from abc import abstractmethod, ABC
from src.save_load.save_load import SaveLoad


class ISaveLoad(ABC, SaveLoad):

    @abstractmethod
    def __init__(self, extension, version, name=""):
        super().__init__(extension, version, name)

    @abstractmethod
    def init_default_values(self):
        pass

    # region save/load

    # Save object
    @abstractmethod
    def save_object(self, file_path=""):
        pass

    # Load Object
    @abstractmethod
    def load_object(self, file):
        pass
