from src.ISaveLoad import ISaveLoad
"""Please use the following example to generate a class."""


class DummyClass(ISaveLoad):
    """

          Creates an instance of the Aerodynamics_D class

          :param air_density: air density [Kg/m^3]
          :param downforce_distribution: Downforce distribution [float][0 - 1]
          :param frontal_area: Vehicle front area [m^2]
          :param downforce_coefficient: Downforce coefficient [int]
          :param name: Name of the object [str]

          Example::

              obj = Aerodynamics_D()
          """
    def __init__(self, name="", air_density=0, downforce_distribution=0, frontal_area=0,
                 downforce_coefficient=0):
        super().__init__(".aer", "0.0.0", name)
        self.air_density = air_density
        self.downforce_distribution = downforce_distribution
        self.frontal_area = frontal_area
        self.downforce_coefficient = downforce_coefficient

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.downforce_coefficient = 2.2
        self.frontal_area = 1.75
        self.air_density = 1.23
        self.downforce_distribution = 0.46

    # region save/load

    # Save object

    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version.public,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "air_density": self.air_density,
            "downforce_distribution": self.downforce_distribution,
            "frontal_area": self.frontal_area,
            "downforce_coefficient": self.downforce_coefficient,
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file

    # Load Object
    def load_object(self, file):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.aer") #Load

        """
        data = self.load_from_json(file)

        # Get the parameters from the json file.
        self.air_density = data.get("air_density")
        self.downforce_distribution = data.get("downforce_distribution")
        self.frontal_area = data.get("frontal_area")
        self.downforce_coefficient = data.get("downforce_coefficient")

    # endregion


def test_ISaveLoad():
    dummy_aero = DummyClass()
    print(dummy_aero.current_date_time())




